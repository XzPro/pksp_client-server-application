const router= require('express').Router();
const User=require('../model/User');
const jwt=require('jsonwebtoken');
const bcrypt=require('bcryptjs');
const {RegisterValidation, LoginValidation}=require('../validation');

//registration of users
router.post('/register',async (req, res) => {
    
    console.log("LOG::POST_RequestBody:", req.body);
//validate input data
    const result = RegisterValidation(req.body);
if (result.error) {
    //return res.status(400).json({ error: result.error });
    return res.status(400).send(result.error.details[0].message);
  }
  const emailExist=await User.findOne({email:req.body.email});
  const userNameExist=await User.findOne({email:req.body.name});
  if(emailExist) return res.status(400).send('Email already exist');
  if(userNameExist) return res.status(400).send('UserName already exist');
  
  //hash pass
  const salt=await bcrypt.genSalt(12);
  const hashedPass= await bcrypt.hash(req.body.password, salt);
  
  //Create New User Object
     const user = new User({
        name:   req.body.name,
        email:  req.body.email,
        password: hashedPass
    });
try{
    const savedUser = await user.save();
    //res.send(savedUser);
    res.send({user:user._id});
}catch(err){
    res.status(400).send(err);
}
});
//LOGIN
router.post('/login',async(req,res)=>{
    //validate input data
    const result = LoginValidation(req.body);
if (result.error) {
    //return res.status(400).json({ error: result.error });
    return res.status(400).send(result.error.details[0].message);
  }
  const user=await User.findOne({email:req.body.email});
    if(!user) return res.status(400).send('Email is not found');
  const ValidPass= await bcrypt.compare(req.body.password, user.password );
  if(!ValidPass) return res.status(400).send('Invalid password');
  
const token=jwt.sign({_id:user._id}, process.env.TOKEN_SECRET,{expiresIn:'15d'});
res.header('auth_token',token).send(token);

});
module.exports = router;