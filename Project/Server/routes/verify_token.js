const jwt=require('jsonwebtoken');

module.exports=function valid(req,res,next){
    const token=req.header('user-key');
    if(!token) return res.status(401).send('Access Denied');

    try{
        const verified=jwt.verify(token,process.env.TOKEN_SECRET);
        req.user_id=verified.user_id;
        next();
    }catch(err){
        res.status(400).send('Invalid Token');
    }
}