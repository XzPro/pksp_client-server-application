const router= require('express').Router();
const fbUser=require('../model/fb_user');
const jwt=require('jsonwebtoken');
const FirebaseValidation=require('../validation').FirebaseValidation;

async function createUser(user){
    newUser = fbUser({
        name:user.name,
        fb_id:user.fb_id
    })
    savedUser = await newUser.save();
    return savedUser
}



router.post("/register",(req,res)=>{
    console.log("[LOG]::Post_Request_FirebaseRegister");
    const validation_result = FirebaseValidation(req.query);
    if (validation_result.error) {
        res.status(400).send("[400] Неверный набор данных");
    } else {
        // Проверку на существование пользователя в системе будет делать firebase
        // Значит нам нужно просто создать пользователя и отправить ему токен
        createUser(req.body).then((newUser)=>{
            var token = jwt.sign({_id:user._id}, process.env.TOKEN_SECRET,{expiresIn:'15d'});
            res.json({token:token,username:newUser.name});
        });
    }

});

router.post("/login",(req,res)=>{
    console.log("[LOG]::Post_Request_FirebaseLogin");
    if (req.body.fb_id){ // Для логина нам нужен только токен FireBase
        fbUser.findOne({fb_id:req.query.fb_id}).then((user)=>{
            if (!user) res.status(401).send("[401] Не найден пользователь");
            else {
                var token = jwt.sign({_id:user._id}, process.env.TOKEN_SECRET,{expiresIn:'15d'});
                res.json({token:token,username:user.name});
            }
        })
    } else {
        res.status(400).send("[400] Неверный набор данных");
    }
});

router.get("/getUserToken",(req,res)=>{

    if (req.query.fb_id){
        var token = jwt.sign({user_id:req.query.fb_id}, process.env.TOKEN_SECRET,{expiresIn:'15d'});
        res.json({token:token});
    } else {
        res.status(400).send("[400] Неверный набор данных");
    }
});


module.exports = router;