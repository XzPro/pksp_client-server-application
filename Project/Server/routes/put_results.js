const router= require('express').Router();
const verify=require('./verify_token');
const course_model = require("../model/courses");

async function put_lesson_test_result(course,lesson_no,user_id){
    var course_doc = await course_model.findOne({codename:course}).exec();
    console.log("COURSE BEFORE DELETE:"+course_doc);
    var user_progress = course_doc.user_progress.filter(prog => prog.user_id === user_id)[0];
    console.log("UP:"+user_progress);
    course_doc.user_progress.remove(user_progress)
    console.log("COURSE AFTER DELETE:"+course_doc)
    console.log("UP:"+user_progress.results);
    user_progress.results[lesson_no]=true;
    console.log("UP+:"+user_progress.results);
    course_doc.user_progress.push(user_progress)
    console.log("COURSE AFTER INSERT"+course_doc);
    course_doc.save();
}

async function put_final_test_result(course,user_id){
    var course_doc = await course_model.findOne({codename:course}).exec();
    console.log("COURSE BEFORE DELETE:"+course_doc);
    var user_progress = course_doc.user_progress.filter(prog => prog.user_id === user_id)[0];
    console.log("UP:"+user_progress);
    course_doc.user_progress.remove(user_progress)
    console.log("COURSE AFTER DELETE:"+course_doc)
    console.log("UP:"+user_progress.final_result);
    user_progress.final_result=true;
    console.log("UP+:"+user_progress.final_result);
    course_doc.user_progress.push(user_progress)
    console.log("COURSE AFTER INSERT"+course_doc);
    course_doc.save();
}


router.put("/putLessonTestResult",verify,(req,res)=>{
    put_lesson_test_result(req.query.course,req.query.lesson_no,req.user_id);
});

router.put("/putFinalResult",verify,(req,res)=>{
    put_final_test_result(req.query.course,req.user_id);
});

module.exports = router;