const router= require('express').Router();
const verify=require('./verify_token');
const course_model = require('../model/courses');
const path = require('path');

router.get('/',verify,(req,res)=>{
res.send(req.user);
});

async function signOnCourse(course,user_id){
    var course_doc = await course_model.findOne({codename:course}).exec();
    console.log(course_doc);
    var user_progress = course_doc.user_progress.filter(prog => prog.user_id === user_id);
    console.log(user_progress);
    if (user_progress.length == 0){
        course_doc.user_progress.push({
            user_id:user_id,
            results:[],
            final_result:false
        });
        console.log(course_doc.user_progress);
        for (var i = 0; i<course_doc.lesson_count; i++){
            course_doc.user_progress[course_doc.user_progress.length-1].results.push(false);
        }
        course_doc.save();
    }
}

router.get("/getCourseResults",verify, async (req,res)=>{
    if (req.query.course) {
        var course_doc = await course_model.findOne({codename:req.query.course}).exec();
        var user_progress = course_doc.user_progress.filter(prog => prog.user_id === req.user_id)[0];
        res.json({user_progress:user_progress.results});
    }
});

router.get("/getLessons",verify,(req,res)=>{
    try {
        console.log(req.query.course);
        const lessons = require("../courses/"+req.query.course+"/lessons.json");
        signOnCourse(req.query.course,req.user_id);
        res.json(lessons)
    } catch (error) {
        res.json({status:400,msg:error})
    }
})

router.get("/getLessonTest",verify,(req,res)=>{
    try {
        const test = require("../courses/"+req.query.course+"/lesson_tests/l"+req.query.lesson_no+"_test.json");
        res.json(test);
    } catch (error) {
        res.json({status:400,msg:"No such course or test"});
    }
})

router.get("/getCourseTest",verify, (req,res)=>{
    try {
        const lessons = require("../courses/"+req.query.course+"/test.json");
        res.json(lessons)
    } catch (error) {
        res.json({status:400,msg:"No such course"})
    }
})

router.get("/getImage", (req,res)=>{
    res.sendFile(path.resolve()+"/img/"+req.query.image+".jpg");
});

router.get("/getVideo", (req,res)=>{
    res.sendFile(path.resolve()+"/video/"+req.query.video+".mp4");
});

module.exports = router;