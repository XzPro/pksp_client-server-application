const router= require('express').Router();
const verify=require('./verify_token');
const courses = require("../model/courses");



async function get_courses_list(){
    console.log("[LOG]::Getting Courses")
    var query = await courses.find().exec();
    return query;
    
}

function setCourseInfo(course,user_id){
    var course_progress = course.user_progress.filter(prog => prog.user_id === user_id);
    var course_info = {}
    course_info.title = course.title;
    course_info.codename = course.codename;
    course_info.category = course.category;
    course_info.desc = course.desc;
    course_info.lesson_count = course.lesson_count;
    course_info.test_time = course.test_time;
    console.log(course_progress);
    if (course_progress[0]) {
        course_info.user_progress = course_progress[0].results;
        course_info.final_result = course_progress[0].final_result;
    }
    console.log(course_info);
    return course_info;
}

router.get("/getActiveCourses",verify,(req,res)=>{
    get_courses_list().then((docs)=>{
        // Возвращаем пользователю документы с прогрессом для него
        var courses_info = []
        docs.forEach(doc => {
            var course_progress = doc.user_progress.filter(prog => prog.user_id === req.user_id);
            if (course_progress.length != 0){
                courses_info.push(setCourseInfo(doc,req.user_id));
            }
        });
        res.json(courses_info);
    });
})


router.get("/getCourses",verify,(req,res)=>{
    get_courses_list().then((docs)=>{
        // Возвращаем пользователю документы с прогрессом для него
        var courses_info = []
        docs.forEach(doc => {
            courses_info.push(setCourseInfo(doc,req.user_id));
        });
        res.json(courses_info);
    });
})

module.exports = router;