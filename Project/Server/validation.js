//import joi
/* joi is The most powerful schema description language and data validator */
const Joi = require('@hapi/joi');

const RegisterValidation=(data)=>{
     //joi validation schema
    const Sschema=Joi.object().keys({
    name: Joi.string()
    .min(6)
    .required(),
    email: Joi.string()
    .required()
    .email(),
    password: Joi.string()
    .min(6)
    .required()

    });
    return Sschema.validate(data);
};

const FirebaseValidation=(data)=>{
    const ValidSchema = Joi.object().keys({
        name: Joi.string().required(),
        fb_id:Joi.string().required()
    });
    return ValidSchema.validate(data)
}

const LoginValidation=(data)=>{
    //joi validation schema
    const Sschema=Joi.object().keys({
    email: Joi.string()
    .required()
    .email(),
    password: Joi.string()
    .min(6)
    .required()
    });

    return Sschema.validate(data);
};

module.exports.FirebaseValidation=FirebaseValidation;
module.exports.LoginValidation=LoginValidation;
module.exports.RegisterValidation=RegisterValidation;
