//import express
/* Express is a minimal and flexible Node.js web application framework that provides a robust set of features for web and mobile applications.*/
const express=require('express');
//create and start express app
const app = express();

//import dotenv
/* Dotenv is a zero-dependency module that loads environment variables from a .env file into process.env. */
const dotenv=require('dotenv').config();

//import mongoose
/* Mongoose is a MongoDB object modeling tool designed to work in an asynchronous environment. */
const mongoose = require('mongoose');

//import auth routes from auth.js
const authRoute=require('./routes/auth');

const postRoute=require('./routes/posts');
const getCourses = require("./routes/get_courses");
const putResults = require("./routes/put_results");

const firebaseRoute=require('./routes/fb_auth');

function MakeConStr(){
    return "mongodb://"+process.env.DB_USER+":"+process.env.DB_PASSWORD+process.env.DB_CLUSTER;
}

//connecting to db
mongoose.connect(MakeConStr(),{useUnifiedTopology: true, useNewUrlParser:true, useCreateIndex: true, autoReconnect:true})
.then(() =>  console.log('LOG::[Server]:Connection to DB successful'))
.catch((err) => console.error("LOG::[Server] ",err));
//create JSON body-parser
app.use(express.json());

app.use('/api/user',authRoute);
app.use('/api/posts',postRoute);
app.use('/api/posts',getCourses);
app.use('/api/puts',putResults);
app.use('/api/firebase',firebaseRoute);

app.listen (process.env.PORT,()=>console.log('LOG::[Server]: I`m Up and running!'));