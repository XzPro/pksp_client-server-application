//import mongoose
const mongoose =require('mongoose');

//create user data validation  schema
/* A Mongoose ‘schema’ is a document data structure that is enforced via the application layer.
Schema maps to a MongoDB collection and defines the shape of the documents within that collection.*/
const coursesSchema=new mongoose.Schema({
/* Схема лекций */    
    title:{ // Название курса
        type:String,
        required:true,
        unique: true,
        min:6,
        max:255
    },
    codename:String,
    category:String,
    desc:String,
    test_time:Number,
    lesson_count:Number,
    user_progress:[{  // Прогресс пользователей
        user_id:String, 
        results:[Boolean],
        final_result:Boolean
    }]
});

module.exports=mongoose.model('courses',coursesSchema);