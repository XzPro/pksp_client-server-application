//import mongoose
const mongoose =require('mongoose');

//create user data validation  schema
/* A Mongoose ‘schema’ is a document data structure that is enforced via the application layer.
Schema maps to a MongoDB collection and defines the shape of the documents within that collection.*/
const firebaseUserSchema=new mongoose.Schema({
name:{
    type:String,
    required:true,
    unique: true,
    min:6,
    max:255
},
fb_id:{
    type:String,
    required:true,
    unique: true
},
date:{
    type:Date,
    default: Date.now
},


});
module.exports=mongoose.model('fbUsers',firebaseUserSchema);