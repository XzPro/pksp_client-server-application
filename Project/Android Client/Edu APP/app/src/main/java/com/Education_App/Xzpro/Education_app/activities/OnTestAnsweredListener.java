package com.Education_App.Xzpro.Education_app.activities;

public interface OnTestAnsweredListener {
    void getAnswer(Boolean result);
}
