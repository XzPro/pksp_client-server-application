package com.Education_App.Xzpro.Education_app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.Education_App.Xzpro.Education_app.adapters.retrofit.APIService;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.ApiUtils;
import com.Education_App.Xzpro.Education_app.models.User;
import com.Education_App.Xzpro.Education_app.R;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class Login extends AppCompatActivity {
    private int RC_SIGN_IN = 0;
    private TextView loginBtn;
    private AutoCompleteTextView emailField;
    private EditText passwordField;
    private  ImageView googleBtn,facebookBtn;
    private  FrameLayout blur;
    private  AVLoadingIndicatorView login_loader;
    private LoginButton FloginButton;
    private static final String TAG = "LoginActivity";

    private CallbackManager callbackManager;
    private AccessToken accessToken;
    private GoogleSignInClient mGoogleSignInClient;
    private FirebaseAuth mAuth;
    //region Changes1
    private APIService mAPIService;
    private User UserFromServer;

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
            FirebaseUser firebaseUser = currentUser;
            getUserDataFromServer(firebaseUser.getUid());
        }
        else Log.d(TAG, "updateUI: Firebase login failed.");
    }
    //endregion
//region FACEBOOK_AUTH
    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        Snackbar.make(findViewById(R.id.facebook_signin_btn), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                        updateUI(null);
                    }

                    // ...
                });
    }
    // [END auth_with_facebook]
    //endregion
//region GOOGLE_AUTH
    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signIn With Google Credential:success");
                        Snackbar.make(findViewById(R.id.google_sign_btn), "Authentication success", Snackbar.LENGTH_SHORT).show();
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signIn With Google Credential:failure", task.getException());
                        Snackbar.make(findViewById(R.id.google_sign_btn), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                        updateUI(null);
                    }

                });
    }

    // [END auth_with_google]
    //endregion
//region EMAIL_AUTH
    private void emailSignIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }
        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, task -> {
                    if (task.isSuccessful()) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithEmail:success");
                        FirebaseUser user = mAuth.getCurrentUser();
                        updateUI(user);
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithEmail:failure", task.getException());
                        Snackbar.make(findViewById(R.id.sign_in_login), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                        updateUI(null);
                    }
                });
        // [END sign_in_with_email]
    }
    //проверяем, корректно ли заполнены поля
    private boolean validateForm() {
        boolean valid = true;

        String email = emailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            emailField.setError("Required.");
            valid = false;
        } else {
            emailField.setError(null);
        }

        String password = passwordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            passwordField.setError("Required.");
            valid = false;
        } else {
            passwordField.setError(null);
        }

        return valid;
    }
    //endregion
    //переопределяем метод создания login-активити
    @Override
    protected void onCreate(Bundle savedInstanceState)      {
        super.onCreate(savedInstanceState);
        //Инициализация объектов view
        setContentView(R.layout.activity_login);
        login_loader = findViewById(R.id.login_loader);
        blur = findViewById(R.id.background_blur_login);
        emailField = findViewById(R.id.Log_email);
        passwordField = findViewById(R.id.Log_password);
        //Изменения
         mAPIService = ApiUtils.getAPIService();
        //[End]Изменения

        //[START config_signin]
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        // [END config_signin]
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // [START initialize_fblogin]
        // Initialize Facebook Login button
        mAuth = FirebaseAuth.getInstance();
        FloginButton = findViewById(R.id.Facelogin_button);
        FloginButton.setPermissions(Arrays.asList("email", "public_profile"));
        callbackManager = CallbackManager.Factory.create();
        // Callback registration
        FloginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                // Retrieving access token using the LoginResult
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                // App code
                updateUI(null);
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
                Log.d(TAG, "facebook:onError: " + exception.toString());
                updateUI(null);
            }
        });
        // [END initialize_fblogin]

        TextView forgetPass = findViewById(R.id.tv_forget_pass);
        forgetPass.setOnClickListener(view->{
            AlertDialog.Builder inputDialog = new AlertDialog.Builder(this);
            inputDialog.setTitle("Введите email для восстановления пароля");
            EditText et_email = new EditText(this);
            inputDialog.setView(et_email);
            inputDialog.setPositiveButton("Отправить",(dialog, i)->
                mAuth.sendPasswordResetEmail(et_email.getText().toString())
                    .addOnCompleteListener(task -> Toast.makeText(getApplicationContext(),"Сообщение было отправлено",Toast.LENGTH_SHORT).show())
                    .addOnFailureListener(task -> Toast.makeText(getApplicationContext(),"Ошибка. Сообщение не отправлено",Toast.LENGTH_SHORT).show()));

            inputDialog.show();
        });

        //событие click, возникшее на pictirebox вызывает событие нажатия на кнопку авторизации FAcebook
        facebookBtn = findViewById(R.id.facebook_signin_btn);
        facebookBtn.setOnClickListener(view -> FloginButton.performClick());

        //гугл листнер
        googleBtn = findViewById(R.id.google_sign_btn);
        googleBtn.setOnClickListener(view -> {
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
        });

        // listner для кнопки LOGIN
        loginBtn = findViewById(R.id.sign_in_login);
        loginBtn.setOnClickListener((v)->emailSignIn(emailField.getText().toString(), passwordField.getText().toString()));

    }

    @Override
    public void onStart() {
        super.onStart();
        // Если пользователь уже авторизован.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        updateUI(currentUser);
        //изменения
        //UserFromServer=loadDataFromSP();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {

            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed
                Log.w(TAG, "Google sign in failed", e);

                updateUI(null);

            }
        }
        //для facebook
        // Передать результат действия обратно в пакет SDK Facebook
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    public void openSignupPage(View view) {
        startActivity(new Intent(this, Register.class));
    }

//region Retrofit

    //сохранение данных пользователя в памяти устройства
    public void  saveDataToSP(User user){

        FirebaseUser fbUser = FirebaseAuth.getInstance().getCurrentUser();
        SharedPreferences.Editor sharedPref = getSharedPreferences("authInfo", Context.MODE_PRIVATE).edit();
        sharedPref.putString("UserName", fbUser.getDisplayName());
        sharedPref.putString("UserID", fbUser.getUid());
        sharedPref.putString("AccessToken", user.getToken());
        //changes 12/16/2019
        sharedPref.putString("ProfileImage", fbUser.getPhotoUrl().toString());
        sharedPref.putString("Email",  fbUser.getEmail());
        sharedPref.apply();

    }

    //запрос данных пользователя с сервера
    private void getUserDataFromServer(String AUTHORIZATION_FIREBASE_ID ) {
        //создаем асинхронный запрос
        mAPIService.getToken(AUTHORIZATION_FIREBASE_ID).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                try {
                    User user;
                    Intent intent = new Intent(Login.this, ProfileActivity.class);
                    user=response.body();
                    saveDataToSP(user);
                    startActivity(intent);
                    finish();
                } catch (Exception e) {
                    Log.d("onResponse", "Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }
    //endregion
}

