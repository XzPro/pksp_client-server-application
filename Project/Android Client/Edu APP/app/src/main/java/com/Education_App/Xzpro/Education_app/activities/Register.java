package com.Education_App.Xzpro.Education_app.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.Education_App.Xzpro.Education_app.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.List;

public class Register extends AppCompatActivity {

    private static final String TAG = "RegisterActivity";

    AVLoadingIndicatorView register_loader;
    TextView registerBtn;
    FrameLayout blur;

    AutoCompleteTextView EmailField;
    EditText PasswordField, ReEnterPasswordField;

    private FirebaseAuth mAuth;

    private void updateUI(FirebaseUser currentUser) {
        if (currentUser != null) {
            List<? extends UserInfo> firebaseUser=(currentUser.getProviderData());
            Intent intent = new Intent(this, Login.class);
            sendEmailVerification();
            startActivity(intent);
        }
        else Log.d(TAG, "updateUI: Firebase login failed.");
    }

    private void sendEmailVerification() {

        // Send verification email
        // [START send_email_verification]
        final FirebaseUser user = mAuth.getCurrentUser();
        user.sendEmailVerification()
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {

                        if (task.isSuccessful()) {
                            Snackbar.make(findViewById(R.id.register_btn), "Verification email sent to " + user.getEmail(), Snackbar.LENGTH_SHORT).show();
                        } else {
                            Log.e(TAG, "sendEmailVerification", task.getException());
                            Snackbar.make(findViewById(R.id.register_btn), "Failed to send verification email.", Snackbar.LENGTH_SHORT).show();
                        }
                    }
                });
        // [END send_email_verification]
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = EmailField.getText().toString();
        if (TextUtils.isEmpty(email)) {
            EmailField.setError("Required.");
            valid = false;
        } else {
            EmailField.setError(null);
        }

        String password = PasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            PasswordField.setError("Required.");
            valid = false;
        } else {
            PasswordField.setError(null);
        }
        String Repassword = ReEnterPasswordField.getText().toString();
        if (TextUtils.isEmpty(Repassword)) {
            ReEnterPasswordField.setError("Required.");
            valid = false;
        } else {
            ReEnterPasswordField.setError(null);
        }

        if(!password.equals(Repassword)){
            ReEnterPasswordField.setError("Required.");
            valid = false;
        }else {
            ReEnterPasswordField.setError(null);
        }
        return valid;
    }

    private void setUserName(FirebaseUser user){

        TextView firstName = findViewById(R.id.first_name);
        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                .setDisplayName(firstName.getText().toString())
                .setPhotoUri(Uri.parse("https://yt3.ggpht.com/a/AGF-l7-2GucjwNfntc0rnjPliEIxabxGFcusJj0q3w=s900-c-k-c0xffffffff-no-rj-mo"))
                .build();

        user.updateProfile(profileUpdates)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            updateUI(user);
                            Log.d(TAG, "User profile updated.");
                        }
                    }
                });


    }

    private void createAccount(String email, String password) {
        Log.d(TAG, "createAccount:" + email);
        if (!validateForm()) {
            return;
        }

        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "createUserWithEmail:success");
                            FirebaseUser user = mAuth.getCurrentUser();
                            setUserName(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Snackbar.make(findViewById(R.id.register_btn), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }
                });
        // [END create_user_with_email]
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        mAuth = FirebaseAuth.getInstance();
        blur = findViewById(R.id.background_blur_register);
        register_loader = findViewById(R.id.register_loader);

        EmailField = findViewById(R.id.email);
        PasswordField = findViewById(R.id.password);
        ReEnterPasswordField = findViewById(R.id.re_password);

        registerBtn = findViewById(R.id.register_btn);
        registerBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                blur.setBackgroundResource(R.drawable.button_background2);
                createAccount(EmailField.getText().toString(), PasswordField.getText().toString());
                /*register_loader.setVisibility(View.VISIBLE);
                register_loader.show();*/
            }
        });


    }

    public void openLoginPage(View view) {
        startActivity(new Intent(this,Login.class));
    }
}
