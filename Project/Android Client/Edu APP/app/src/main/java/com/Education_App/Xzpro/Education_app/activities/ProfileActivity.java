
package com.Education_App.Xzpro.Education_app.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.InputType;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.Education_App.Xzpro.Education_app.adapters.CoursesProgGridAdapter;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.APIService;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.ApiUtils;
import com.Education_App.Xzpro.Education_app.layout.ExpandableHeightGridView;
import com.Education_App.Xzpro.Education_app.models.Course;
import com.Education_App.Xzpro.Education_app.models.User;
import com.Education_App.Xzpro.Education_app.R;
import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends AppCompatActivity {

    //меню на странице профиля
    ListView listView;
    String[] ProfileMenu = new String[] { "Настройки", "О программе",   "Выход"};
    APIService mAPI;
    SharedPreferences sharedPref;


    //основное меню
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Intent intent;
            switch (item.getItemId()) {

                case R.id.navigation_home:
                    intent = new Intent(ProfileActivity.this, HomeActivity.class);
                    // TODO: Придумать что-то поадекватнее
                    startActivity(intent);
                    finish();
                    return true;
                case R.id.navigation_profile:

                    return true;
            }
            return false;
        }
    };

    //загрузка данных пользователя из памяти устройства
    public User loadDataFromSP(){

        User user = new User();
        sharedPref = getSharedPreferences("authInfo", Context.MODE_PRIVATE);
        user.setId(sharedPref.getString("UserID", ""));
        user.setUserName(sharedPref.getString("UserName", ""));
        user.setToken(sharedPref.getString("AccessToken", ""));
        user.setImage( sharedPref.getString("ProfileImage",""));
        user.setEmail( sharedPref.getString("Email",""));
        return user;
    }


    private void updateUI(User user){
        if(user!=null){
        CircleImageView profile_image = findViewById(R.id.iv_profile_image);
        TextView profile_name = findViewById(R.id.tv_profile_name);
        Glide.with(this)
                .load(user.getImage())
                .into(profile_image);
        profile_name.setText(user.getUserName());
    }
            }


    private void showSignOutAlert() {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.sign_out_dialog_title))
                .setMessage(getString(R.string.sign_out_message))
                .setPositiveButton(getResources().getString(R.string.sign_out_ok), (dialog, which) -> {
                    //googleLogOut

                    GoogleSignInClient mGoogleSignInClient ;
                    GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                            .requestIdToken(getString(R.string.default_web_client_id))
                            .requestEmail()
                            .build();
                    mGoogleSignInClient = GoogleSignIn.getClient(getBaseContext(), gso);

                    mGoogleSignInClient.signOut();
                    // Google revoke access
                    mGoogleSignInClient.revokeAccess();


                    LoginManager.getInstance().logOut();
                    try {
                        FirebaseUser currentUser = FirebaseAuth.getInstance().getCurrentUser();
                        if (currentUser != null) {
                            FirebaseAuth.getInstance().signOut();
                            Toast.makeText(getBaseContext(), "Logged Out", Toast.LENGTH_LONG).show();
                            Intent signInIntent = new Intent(this, Login.class);
                            signInIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            updateUI(null);
                            sharedPref.edit().clear().apply();
                            startActivity(signInIntent);
                            this.finishAffinity();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                })
                .setNegativeButton(getString(R.string.sign_out_cancel), (dialog, which) -> dialog.dismiss())
                .create().show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        User user = loadDataFromSP();

        setContentView(R.layout.activity_profile);
        updateUI(user);
        mAPI = ApiUtils.getAPIService();

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        navigation.setSelectedItemId(R.id.navigation_profile);
        // инициализироем меню на странице профиля
        listView = (ListView) findViewById(R.id.ProfileMenu_listView);
        ArrayAdapter adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, ProfileMenu);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                String str=(String)parent.getItemAtPosition(position);
                switch (str){
                    case "Настройки":
                        // TODO: Настроечки
                        showSettingsDialog();
                        break;
                    case "О программе":
                        showAbout();
                        break;
                    case "Выход":
                        showSignOutAlert();
                        break;
                }

            }
        });


        loadActiveCourses();

    }

    private void setNewUsername(){
        AlertDialog.Builder newUsername = new AlertDialog.Builder(this);
        newUsername.setTitle("Введите новое имя");
        EditText etNewUsername = new EditText(this);
        newUsername.setView(etNewUsername);
        newUsername.setPositiveButton("Изменить",(dialog,i) -> {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                    .setDisplayName(etNewUsername.getText().toString()).build();
            user.updateProfile(profileUpdates)
                    .addOnCompleteListener(task -> {
                        Toast.makeText(getApplicationContext(),"Имя изменено",Toast.LENGTH_SHORT);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("UserName",user.getDisplayName());
                        editor.apply();
                        updateUI(loadDataFromSP());
                    })
                    .addOnFailureListener(task->Toast.makeText(getApplicationContext(),"Ошибка!",Toast.LENGTH_SHORT));
        });
        newUsername.setNegativeButton("Отмена",(dialog,i)->{});
        newUsername.show();
    }

    private void setNewPassword(){
        AlertDialog.Builder newUsername = new AlertDialog.Builder(this);
        newUsername.setTitle("Введите новый пароль");
        EditText etNewPassword = new EditText(this);
        etNewPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        newUsername.setView(etNewPassword);
        newUsername.setPositiveButton("Изменить",(dialog,i) -> {
            FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
            user.updatePassword(etNewPassword.getText().toString())
                    .addOnCompleteListener(task ->Toast.makeText(getApplicationContext(),"Пароль успешно изменен",Toast.LENGTH_SHORT))
                    .addOnFailureListener(task->Toast.makeText(getApplicationContext(),"Ошибка!",Toast.LENGTH_SHORT));
        });
        newUsername.setNegativeButton("Отмена",(dialog,i)->{});
        newUsername.show();
    }

    private void showSettingsDialog(){
        AlertDialog.Builder settings = new AlertDialog.Builder(this);
        AlertDialog settingsDialog;
        settings.setTitle("Настройки");
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        LinearLayout container = new LinearLayout(this);
        container.setOrientation(LinearLayout.VERTICAL);
        Button changeUsername = new Button(this);
        Button changePassword = new Button(this);
        changeUsername.setText("Изменить имя пользователя");
        changeUsername.setLayoutParams(params);
        changePassword.setText("Сменить пароль");
        changePassword.setLayoutParams(params);
        container.addView(changeUsername);
        container.addView(changePassword);
        settings.setView(container);

        settingsDialog = settings.create();

        changeUsername.setOnClickListener(v->{setNewUsername(); settingsDialog.dismiss();});
        changePassword.setOnClickListener(v->{setNewPassword(); settingsDialog.dismiss();});
        settingsDialog.show();


    }


    private void showAbout(){
        AlertDialog.Builder about = new AlertDialog.Builder(this);
        about.setView(R.layout.fragment_about);
        about.setNegativeButton("Закрыть",(dialog,i)->{});
        about.show();
    }
    private void loadActiveCourses(){
        String token = getSharedPreferences("authInfo",MODE_PRIVATE).getString("AccessToken","");
        mAPI.getActiveCourses(token).enqueue(new Callback<ArrayList<Course>>() {
            @Override
            public void onResponse(Call<ArrayList<Course>> call, Response<ArrayList<Course>> response) {
                TextView activeCourses = findViewById(R.id.tv_profile_active_courses);
                TextView tvCompleteCourses = findViewById(R.id.tv_profile_complete_courses);
                if (response.body()==null){
                    activeCourses.setText("0");
                    tvCompleteCourses.setText("0");
                }
                else {
                    ExpandableHeightGridView gridCourses = findViewById(R.id.gridCoursesProg);
                    CoursesProgGridAdapter coursesAdapter = new CoursesProgGridAdapter(response.body());
                    gridCourses.setAdapter(coursesAdapter);
                    gridCourses.setExpanded(true);

                    activeCourses.setText(String.valueOf(response.body().size()));
                    int completeCourses = 0;
                    for (Course course : response.body()) {
                        if (course.getPercentageProgress() >= 100) {
                            completeCourses++;
                        }
                    }

                    tvCompleteCourses.setText(String.valueOf(completeCourses));
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Course>> call, Throwable t) {

            }
        });
    }
}
