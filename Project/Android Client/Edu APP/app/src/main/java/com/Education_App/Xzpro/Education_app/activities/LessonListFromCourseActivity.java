
package com.Education_App.Xzpro.Education_app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.Education_App.Xzpro.Education_app.adapters.LessonListAdapter;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.APIService;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.ApiUtils;
import com.Education_App.Xzpro.Education_app.models.Course;
import com.Education_App.Xzpro.Education_app.models.Lesson;
import com.Education_App.Xzpro.Education_app.models.Question;
import com.Education_App.Xzpro.Education_app.R;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LessonListFromCourseActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    private ListView listView;
    private LessonListAdapter LessonAdapter;
    private APIService mAPIService;
    private ArrayList<Lesson> LessonList;



    //инициализируем View
    private void initView() {
        listView = findViewById(R.id.courses_list);
        listView.setVisibility(View.INVISIBLE);

        mAPIService = ApiUtils.getAPIService();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lesson_list);
        Toolbar toolbar = findViewById(R.id.tb);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LessonListFromCourseActivity.super.onBackPressed();
            }
        });
        getSupportActionBar().setTitle(getIntent().getStringExtra("courseTitle"));
        LessonList = new ArrayList<>();
        initView();

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        String token = getSharedPreferences("authInfo",MODE_PRIVATE).getString("AccessToken","");
        // TODO: Вызов getFinalTest
        if (i == LessonList.size()){
            openFinalTest(token);
        } else {
            openLesson(i,token);
        }
    }

    private void openFinalTest(String token){
        mAPIService.getFinalTest(getIntent().getStringExtra("courseName"),token).enqueue(new Callback<ArrayList<Question>>() {
            @Override
            public void onResponse(Call<ArrayList<Question>> call, Response<ArrayList<Question>> response) {
                Intent finalTest = new Intent(LessonListFromCourseActivity.this, QuizActivity.class);
                finalTest.putExtra("courseName",getIntent().getStringExtra("courseName"));
                finalTest.putParcelableArrayListExtra("test", response.body());
                finalTest.putExtra("que_no",1);
                finalTest.putExtra("test_time",getIntent().getIntExtra("test_time",0));
                startActivity(finalTest);//запускаем активити для обучения
            }

            @Override
            public void onFailure(Call<ArrayList<Question>> call, Throwable t) {
                t.toString();

            }
        });
    }

    private void openLesson(int pos, String token){
        Intent lessonDetail = new Intent(LessonListFromCourseActivity.this,LessonDetailActivity.class);
        lessonDetail.putExtra("courseName",getIntent().getStringExtra("courseName"));
        lessonDetail.putExtra("lesson", LessonList.get(pos));
        lessonDetail.putExtra("lesson_no",pos);
        if (LessonList.get(pos).getWithTest()) {
            mAPIService.getTest(getIntent().getStringExtra("courseName"), pos + 1, token).enqueue(new Callback<ArrayList<Question>>() {
                @Override
                public void onResponse(Call<ArrayList<Question>> call, Response<ArrayList<Question>> response) {
                    //получаем элементы по наименованию
                    lessonDetail.putParcelableArrayListExtra("test", response.body());
                    startActivity(lessonDetail);//запускаем активити для обучения
                }

                @Override
                public void onFailure(Call<ArrayList<Question>> call, Throwable t) {  t.toString();    }
            });
        } else {
            startActivity(lessonDetail);//запускаем активити для обучения
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getLessonArray(getIntent().getStringExtra("courseName"));
    }

    private void setListView() {

        String token = getSharedPreferences("authInfo",MODE_PRIVATE).getString("AccessToken","");
        mAPIService.getCourseResults(getIntent().getStringExtra("courseName"),token).enqueue(new Callback<Course>() {
            @Override
            public void onResponse(Call<Course> call, Response<Course> response) {

                boolean finalTestAccess = response.body().getPercentageProgress()==100;
                if (finalTestAccess)
                    Snackbar.make(listView,"Вам доступен финальный тест!",Snackbar.LENGTH_INDEFINITE).show();

                LessonAdapter = new LessonListAdapter(LessonListFromCourseActivity.this, LessonList, finalTestAccess);
                listView.setAdapter(LessonAdapter);

                listView.setVisibility(View.VISIBLE);

                listView.setOnItemClickListener(LessonListFromCourseActivity.this);
            }

            @Override
            public void onFailure(Call<Course> call, Throwable t) {
                t.toString();
            }
        });

    }

    private void getLessonArray(String courseName) {
        //создаем асинхронный запрос
        String token = getSharedPreferences("authInfo",MODE_PRIVATE).getString("AccessToken","");
        mAPIService.GetLessonsOfCourse(courseName,token).enqueue(new Callback<ArrayList<Lesson>>() {//hardcode for test
            @Override
            public void onResponse(Call<ArrayList<Lesson>> call, Response<ArrayList<Lesson>> response) {
                try {
                    LessonList = response.body();//записываем в список то, что вернул response
                    setListView();

                } catch (Exception e) {
                    Log.d("onResponse", "Error");
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Lesson>> call, Throwable t) {
                Log.d("onFailure", t.toString());
            }
        });
    }

}
