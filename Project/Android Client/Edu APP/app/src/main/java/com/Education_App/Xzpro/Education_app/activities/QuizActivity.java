package com.Education_App.Xzpro.Education_app.activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.Education_App.Xzpro.Education_app.adapters.retrofit.APIService;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.ApiUtils;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_InputQuizFragment;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_MultiAnswerQuizFragment;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_QuizFragment;
import com.Education_App.Xzpro.Education_app.models.Question;
import com.Education_App.Xzpro.Education_app.R;

import org.json.JSONException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 * Класс предназначен для создания окна Тестирования.
 * Включает функцию onCreate,которая создает activity и устанавливает toolbar
 *
 */
public class QuizActivity extends AppCompatActivity implements OnTestAnsweredListener{

    private Button btnPrevious;
    private Fragment fragment;
    private Button btnNext;
    private LinearLayout llDirectionQuiz;
    private TextView tvQuestionNum;
    TextView tvSubQuestion;
    TextView tvQuestion;
    private APIService mApiService;

    private int questionNo;
    private ArrayList<Question> questionList;
    private int quizNum;
    private ArrayList<Boolean> userAnswerList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);
        mApiService = ApiUtils.getAPIService();
        btnPrevious = findViewById(R.id.btn_previous);
        btnNext = findViewById(R.id.btn_next);
        llDirectionQuiz = findViewById(R.id.ll_direction_quiz);
        tvQuestionNum = findViewById(R.id.question_number);
        tvQuestion = (TextView) findViewById(R.id.tv_question);
        init();

    }

    private void init() {
        questionList = getIntent().getParcelableArrayListExtra("test");
        // Для хранения вопросов
        userAnswerList = new ArrayList<>();
        quizNum = getIntent().getIntExtra("que_no",1);
        CountDownTimer timer = new CountDownTimer(getIntent().getIntExtra("test_time",0),1000) {
            @Override
            public void onTick(long l) {
                TextView tv_timer = findViewById(R.id.tv_timer);
                long hours =  TimeUnit.MILLISECONDS.toHours(l);
                long minutes = TimeUnit.MILLISECONDS.toMinutes(l) - TimeUnit.HOURS.toMinutes(TimeUnit.MICROSECONDS.toHours(l));
                long seconds = TimeUnit.MILLISECONDS.toSeconds(l) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(l));
                tv_timer.setText(String.format("Времени осталось: %02d:%02d:%02d", hours, minutes, seconds));
            }

            @Override
            public void onFinish() {
                submitQuiz();
            }
        };
        timer.start();
        displayQuiz(quizNum);
    }
                //отображаем вопрос
    private void displayQuiz(int questionNo) {


        // Если 1 вопрос, то нет кнопки превью
        if (questionNo == 1) {
            llDirectionQuiz.setWeightSum(1);
            btnPrevious.setVisibility(View.GONE); //скрываем кнопку
        } else {
            llDirectionQuiz.setWeightSum(2);
            btnPrevious.setVisibility(View.VISIBLE);
            btnPrevious.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userAnswerList.size() < quizNum) {
                        Toast.makeText(getApplicationContext(), getString(R.string.required_answer_next), Toast.LENGTH_SHORT).show();
                    } else {
                        previous(view);
                    }


                }
            });
            btnPrevious.setText(getString(R.string.btn_previous));
        }

        // Если последний вопрос
        if (questionNo == questionList.size()) {
            btnNext.setText(R.string.btn_submit);
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (userAnswerList.size() < quizNum) {
                        Toast.makeText(getApplicationContext(), getString(R.string.required_answer_next), Toast.LENGTH_SHORT).show();
                    } else {
                        submitQuiz();
                    }
                }
            });
        } else {
            btnNext.setText(getString(R.string.btn_next));
            btnNext.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                     if (userAnswerList.size() < quizNum) {
                            Toast.makeText(getApplicationContext(), getString(R.string.required_answer_next), Toast.LENGTH_SHORT).show();
                    } else {
                        next(view);
                    }

                }
            });
        }

        // Устанавливаем для отображения номер вопроса
        tvQuestionNum.setText(String.format(getString(R.string.question_num), questionNo, questionList.size()));

        // Получаем объект-вопрос по номеру
        Question question = questionList.get(questionNo - 1);

        switch (question.getType()){
            case "single":
                fragment = vp_QuizFragment.newInstance(question,this);
                break;
            case "multi":
                fragment = vp_MultiAnswerQuizFragment.newInstance(question,this);
                break;
            case "input":
                fragment = vp_InputQuizFragment.newInstance(question,this);
                break;
        }


        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.fl_fragment,fragment);
        ft.commit();
        // Устанавливаем  вопрос


    }

    @Override
    public void getAnswer(Boolean result) {
        if (userAnswerList.size()>=quizNum) {
            userAnswerList.set(quizNum - 1, result);
        } else {
            userAnswerList.add(result);
        }
    }

    public void showResults(){
        Intent results = new Intent(QuizActivity.this,ResultActivity.class);
        results.putExtra("results",userAnswerList);
        results.putExtra("quizSize",questionList.size());
        startActivity(results);
    }

    //переход на предыдущий вопрос
    public void previous(View view) {
        displayQuiz(--quizNum);
    }
            //переход на следующий вопрос
    public void next(View view){
        displayQuiz(++quizNum);
    }
                //если заканчиваем тест
    private void submitQuiz() {
        float right_answers = 0;
        for (Boolean result:userAnswerList){
            if (result) right_answers++;
        }

        if (right_answers/questionList.size() > 0.5){
            AlertDialog.Builder good_boy = new AlertDialog.Builder(this)
                    .setTitle("Отлично!")
                    .setMessage("Вы успешно прошли тест")
                    .setCancelable(false)
                    .setPositiveButton("Хорошо", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // TODO: Отправка результата на серв
                            // TODO: Переход на активити с результатом
                            String token = getSharedPreferences("authInfo",MODE_PRIVATE).getString("AccessToken","");
                            mApiService.putFinalTestResult(getIntent().getStringExtra("courseName"),token).enqueue(new Callback<String>() {
                                @Override
                                public void onResponse(Call<String> call, Response<String> response) {

                                }

                                @Override
                                public void onFailure(Call<String> call, Throwable t) {

                                }
                            });
                            showResults();
                            finish();
                        }
                    });
            AlertDialog dialog = good_boy.create();
            dialog.show();
        } else {
            AlertDialog.Builder not_so_good_boy = new AlertDialog.Builder(this)
                    .setTitle("Неудача!")
                    .setMessage("Вы успешно провалили тест!")
                    .setCancelable(false)
                    .setPositiveButton(":c", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            // TODO: Переход на активити с результатом
                            showResults();
                            finish();
                        }
                    });
            AlertDialog dialog = not_so_good_boy.create();
            dialog.show();
        }


    }
}