package com.Education_App.Xzpro.Education_app.layout.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import androidx.fragment.app.Fragment;

import com.Education_App.Xzpro.Education_app.R;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

import android.view.ViewGroup;
import android.widget.Toast;

public class vp_VideoplayerLesson_fragment extends Fragment{

    private JCVideoPlayerStandard jcVideoPlayerStandard;
    static final String ARGUMENT_PAGE_CONTENT = "arg_page_content";
    static final String ARGUMENT_PAGE_TITLE = "arg_page_title";

    public static vp_VideoplayerLesson_fragment newInstance(String url, String videoTitle) {
        vp_VideoplayerLesson_fragment pageFragment = new vp_VideoplayerLesson_fragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_PAGE_CONTENT,url);
        arguments.putString(ARGUMENT_PAGE_TITLE,videoTitle);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vp_videoplayer_fragment, null);
        jcVideoPlayerStandard = (JCVideoPlayerStandard) view.findViewById(R.id.custom_videoplayer_standard);
        jcVideoPlayerStandard.setUp(getArguments().getString(ARGUMENT_PAGE_CONTENT),
                getArguments().getString(ARGUMENT_PAGE_TITLE));
        return view;
    }
    @Override
    public void onPause() {
        super.onPause();
        JCVideoPlayer.releaseAllVideos();
    }
}
