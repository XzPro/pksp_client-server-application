package com.Education_App.Xzpro.Education_app.layout.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.Education_App.Xzpro.Education_app.activities.LessonDetailActivity;
import com.Education_App.Xzpro.Education_app.activities.OnTestAnsweredListener;
import com.Education_App.Xzpro.Education_app.models.Question;
import com.Education_App.Xzpro.Education_app.R;

import java.util.ArrayList;

public class vp_InputQuizFragment extends Fragment {
    public static final String ARGUMENT_QUESTION = "arg_question";
    public static final String ARGUMENT_ANSWERS = "arg_answers";
    public static final String ARGUMENT_TRUE_ANSWER = "arg_true_answer";
    protected OnTestAnsweredListener subscriber;
    ArrayList<CheckBox> checkBoxGroup;

    public static vp_InputQuizFragment newInstance(Question question, OnTestAnsweredListener subscriber) {
        vp_InputQuizFragment pageFragment = new vp_InputQuizFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_QUESTION, question.getTitle());
        arguments.putString(ARGUMENT_TRUE_ANSWER,question.getRightAnswer());
        pageFragment.setArguments(arguments);
        pageFragment.subscriber = subscriber;
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkBoxGroup = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_input_quiz, null);
        TextView tv_question = view.findViewById(R.id.tv_question);
        tv_question.setText(getArguments().getString(ARGUMENT_QUESTION));
        TextView tv_result = view.findViewById(R.id.tv_result);
        EditText et_answer = view.findViewById(R.id.et_answer);
        view.findViewById(R.id.btn_answer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (et_answer.getText().length() == 0) return;
                String right_answer = getArguments().getString(ARGUMENT_TRUE_ANSWER);
                String resultString;
                if (et_answer.getText().toString().toLowerCase().equals(right_answer.toLowerCase())){
                    subscriber.getAnswer(true);
                    resultString = "Правильный ответ! ";
                } else {
                    subscriber.getAnswer(false);
                    resultString = "Неправильный ответ! ";
                    resultString += "Правильный ответ: "+right_answer;
                }

                if (subscriber instanceof LessonDetailActivity)
                    tv_result.setText(resultString);
                view.setEnabled(false);
            }
        });


        return view;
    }



}
