package com.Education_App.Xzpro.Education_app.models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 *Класс, описывающий содержимое курсов
 */
public class Course {
    @SerializedName("_id")
    private String id; // id курса
    @SerializedName("title")
    private String title;   //Название курса
    @SerializedName("codename")
    String codename;
    @SerializedName("category")
    private String category; // Категория

    @SerializedName("test_time")
    private int test_time; // Время на тест в минутах

    private String Description;
    @SerializedName("lesson_count")
    private  int LessonCount;
    @SerializedName("user_progress")
    private ArrayList<Boolean> percentProgress; //Процент прохождения курса
    @SerializedName("final_result")
    private Boolean courseTestResult; // Результат прохождения теста курса
    //Конструкторы
    public Course(String id, String title) {
        this.id = id;
        this.title = title;
    }

    public Course(String title, ArrayList<Boolean> percentProgress) {
        this.title = title;
        this.percentProgress = percentProgress;
    }
        //Гетеры\Сеттеры
    public String getId() {
        return id;
    }

    public int getTestTime() {
        return test_time*60*1000;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getLessonCount() {
        return LessonCount;
    }

    public void setLessonCount(int LessonCount) {
        this.LessonCount = LessonCount;
    }

    public String getCodename() {
        return codename;
    }

    public void setCodename(String codename) {
        this.codename = codename;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public float getPercentageProgress() {

        float total_score = 0;
        for (Boolean result : percentProgress){
            if (result) total_score++;
        }
        return total_score*100/percentProgress.size();
    }

    public void setPercentageProgress(ArrayList<Boolean> percentProgress) {
        this.percentProgress = percentProgress;
    }
}
