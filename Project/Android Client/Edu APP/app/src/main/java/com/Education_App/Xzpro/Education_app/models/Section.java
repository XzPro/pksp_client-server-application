package com.Education_App.Xzpro.Education_app.models;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 *Класс, описывающий темы -страницы лекции
 */

public class Section {
    private int lessonId; //id лекции
    private int sectionN;   //номер секции в лекции
    private String title;   //название секции
    private String text;    //содержимое лекции
    private String lessonTitle; // название Лекции
    private int lessonSections; //кол-во тем-страниц в лекции

//Конструктор
    public Section(int lessonId, int sectionN, String title, String text, String lessonTitle, int lessonSections) {
        this.lessonId = lessonId;
        this.sectionN = sectionN;
        this.title = title;
        this.text = text;
        this.lessonTitle = lessonTitle;
        this.lessonSections = lessonSections;
    }

//Гетеры/сеттеры
    public int getLessonId() {
        return lessonId;
    }

    public void setLessonId(int lessonId) {
        this.lessonId = lessonId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getLessonTitle() {
        return lessonTitle;
    }

    public void setLessonTitle(String lessonTitle) {
        this.lessonTitle = lessonTitle;
    }

    public int getSectionN() {
        return sectionN;
    }

    public void setSectionN(int sectionN) {
        this.sectionN = sectionN;
    }

    public void setLessonSections(int lessonSections) {
        this.lessonSections = lessonSections;
    }

    public int getLessonSections() {
        return lessonSections;
    }
}
