package com.Education_App.Xzpro.Education_app.layout.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.Education_App.Xzpro.Education_app.activities.LessonDetailActivity;
import com.Education_App.Xzpro.Education_app.activities.OnTestAnsweredListener;
import com.Education_App.Xzpro.Education_app.models.Question;
import com.Education_App.Xzpro.Education_app.R;

public class vp_QuizFragment extends Fragment {

    public static final String ARGUMENT_QUESTION = "arg_question";
    public static final String ARGUMENT_ANSWERS = "arg_answers";
    public static final String ARGUMENT_TRUE_ANSWER = "arg_true_answer";
    protected OnTestAnsweredListener subscriber;
    boolean block = false;

    public static vp_QuizFragment newInstance(Question question, OnTestAnsweredListener subscriber) {
        vp_QuizFragment pageFragment = new vp_QuizFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_QUESTION, question.getTitle());
        arguments.putStringArrayList(ARGUMENT_ANSWERS,question.getAnswers());
        arguments.putString(ARGUMENT_TRUE_ANSWER,question.getRightAnswer());
        pageFragment.setArguments(arguments);
        pageFragment.subscriber = subscriber;
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_quiz, null);
        TextView tv_question = view.findViewById(R.id.tv_question);
        RadioGroup radioGroup = (RadioGroup) view.findViewById(R.id.fragment_quiz_rg);

        tv_question.setText(getArguments().getString(ARGUMENT_QUESTION));
        int count = radioGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View obj = radioGroup.getChildAt(i);
            if (obj instanceof RadioButton) {
                RadioButton rb = (RadioButton) obj;
                rb.setText(getArguments().getStringArrayList(ARGUMENT_ANSWERS).get(i));
            }
        }
        // Проверка нажатия на RadioButton
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = (RadioButton) group.findViewById(checkedId);
                TextView result = view.findViewById(R.id.tv_result);
                if (rb != null && checkedId != -1) {
                    if (rb.getText().equals(getArguments().getString(ARGUMENT_TRUE_ANSWER))) {

                        if (subscriber instanceof LessonDetailActivity){
                            rb.setBackgroundColor(Color.GREEN);
                            result.setText("Вы дали правильный ответ");
                        }
                        if (!block) subscriber.getAnswer(true);
                    } else {

                        if (subscriber instanceof LessonDetailActivity){
                            rb.setBackgroundColor(Color.RED);
                            result.setText("Вы ошиблись. Правильный ответ: "+getArguments().getString(ARGUMENT_TRUE_ANSWER));
                        }

                        if (!block) subscriber.getAnswer(false);
                    }
                    //todo: сделать блокировку повторного выбора
                    if (subscriber instanceof LessonDetailActivity) {
                        blockRadioGroup(group);
                    }
                }
            }
        });


        return view;
    }
    private void blockRadioGroup(RadioGroup group) {
        for (int i = 0; i < group.getChildCount(); i++) {
            View obj = group.getChildAt(i);
            RadioButton rb = (RadioButton) obj;
            rb.setClickable(false);
            block = true;

        }
    }
}
