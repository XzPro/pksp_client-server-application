package com.Education_App.Xzpro.Education_app.activities;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.Education_App.Xzpro.Education_app.adapters.retrofit.APIService;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.ApiUtils;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_InputQuizFragment;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_MultiAnswerQuizFragment;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_QuizFragment;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_VideoplayerLesson_fragment;
import com.Education_App.Xzpro.Education_app.layout.fragments.vp_WebViewLesson_fragment;
import com.Education_App.Xzpro.Education_app.models.Lesson;
import com.Education_App.Xzpro.Education_app.models.Question;
import com.Education_App.Xzpro.Education_app.R;

import java.util.ArrayList;

import fm.jiecao.jcvideoplayer_lib.JCVideoPlayer;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LessonDetailActivity extends FragmentActivity implements OnTestAnsweredListener{

    protected ViewPager pager;
    protected PagerAdapter pagerAdapter;
    protected static final String TAG = "myLogs";
    protected int PAGE_COUNT;
    protected ArrayList<Boolean> answers;
    private APIService mApiService;
    protected Button endLessonButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.viewpager);
        mApiService = ApiUtils.getAPIService();
        endLessonButton = findViewById(R.id.btn_check_end);
        answers = new ArrayList<>();
        Lesson lesson  = (Lesson) getIntent().getParcelableExtra("lesson");
        PAGE_COUNT = lesson.getSections().size();
        pager = (ViewPager) findViewById(R.id.Vpager);

        if (getIntent().hasExtra("test")) {
            ArrayList<Question> test = getIntent().getParcelableArrayListExtra("test");
            pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),lesson,test );
            PAGE_COUNT+=test.size();
        } else {
            pagerAdapter = new MyFragmentPagerAdapter(getSupportFragmentManager(),lesson);
        }


        endLessonButton.setOnClickListener((view -> {

           sendResultOnServer();
           finish();

        }));

        pager.setAdapter(pagerAdapter);


        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected, position = " + position);
            }

            @Override
            public void onPageScrolled(int position, float positionOffset,
                                       int positionOffsetPixels) {
                JCVideoPlayer.releaseAllVideos();
                if (!lesson.getWithTest() && position == PAGE_COUNT-1) endLessonButton.setVisibility(View.VISIBLE);
                else endLessonButton.setVisibility(View.GONE);
            }

            @Override
            public void onPageScrollStateChanged(int state) {}
        });
    }

    private void sendResultOnServer(){
        String courseName = getIntent().getStringExtra("courseName");
        int lesson_no = getIntent().getIntExtra("lesson_no",0);
        String token = getSharedPreferences("authInfo",MODE_PRIVATE).getString("AccessToken","");
        mApiService.putLessonTestResult(courseName,lesson_no,token).enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {}

            @Override
            public void onFailure(Call<String> call, Throwable t) {}
        });
    }

    public void checkResult(){
        float testSize =  getIntent().getParcelableArrayListExtra("test").size();
        float right_answers = 0;
        for (Boolean result:answers){
            if (result) right_answers++;
        }
        if (right_answers/testSize > 0.5) {
            AlertDialog.Builder good_boy = new AlertDialog.Builder(this)
                    .setTitle("Отлично!")
                    .setMessage("Вы успешно прошли тест")
                    .setCancelable(false)
                    .setPositiveButton("Хорошо", (DialogInterface dialogInterface, int i) -> {
                            // TODO: Отправка результата на серв
                            sendResultOnServer();
                            finish();
                        });
            AlertDialog dialog = good_boy.create();
            dialog.show();
        } else {
            AlertDialog.Builder not_so_good_boy = new AlertDialog.Builder(this)
                    .setTitle("Неудача!")
                    .setMessage("Вы успешно провалили тест!")
                    .setCancelable(false)
                    .setPositiveButton("ОК.", (DialogInterface dialogInterface, int i) -> {
                            finish();
                        });
            AlertDialog dialog = not_so_good_boy.create();
            dialog.show();
        }
    }

    public void getAnswer(Boolean result){
        answers.add(result);
        if (answers.size() == getIntent().getParcelableArrayListExtra("test").size()){
            checkResult();
        }
    }

    private class MyFragmentPagerAdapter extends FragmentPagerAdapter {

        private Lesson lesson;
        private ArrayList<Question> test;

        public MyFragmentPagerAdapter(FragmentManager fm, Lesson lesson) {
            super(fm);
            this.lesson = lesson;
            this.test = new ArrayList<>();
        }

        public MyFragmentPagerAdapter(FragmentManager fm, Lesson lesson, ArrayList<Question> test){
            super(fm);
            this.lesson = lesson;
            this.test = test;
        }

        @Override
        public Fragment getItem(int position) {
            if (position >= lesson.getSections().size()) {
                Question question = test.get(position-lesson.getSections().size());
                switch (question.getType()){
                    case "single":
                        return vp_QuizFragment.newInstance(question,LessonDetailActivity.this);
                    case "multi":
                        return vp_MultiAnswerQuizFragment.newInstance(question, LessonDetailActivity.this);
                    case "input":
                        // TODO: Фрагмент для ввода
                        return vp_InputQuizFragment.newInstance(question,LessonDetailActivity.this);
                }

            } else {
                switch (lesson.getSectionTypes().get(position)){
                    case "text":
                        return vp_WebViewLesson_fragment.newInstance(lesson.getSections().get(position));
                    case "video":
                        return vp_VideoplayerLesson_fragment.newInstance(lesson.getSections().get(position),lesson.getSectionTitles().get(position));
                }

            }
            return null;
        }

        @Override
        public String getPageTitle(int pos) {
            if (pos >= lesson.getSections().size()) {
                return "Вопрос "+(pos-lesson.getSections().size()+1)+"/"+test.size();
            } else {
                return lesson.getSectionTitles().get(pos);
            }
        }

        @Override
        public int getCount() {
            return PAGE_COUNT;
        }

    }
}
