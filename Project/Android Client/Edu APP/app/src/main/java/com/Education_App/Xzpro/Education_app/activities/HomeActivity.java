package com.Education_App.Xzpro.Education_app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Education_App.Xzpro.Education_app.adapters.recyclerview.RecyclerViewDataAdapter;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.APIService;
import com.Education_App.Xzpro.Education_app.adapters.retrofit.ApiUtils;
import com.Education_App.Xzpro.Education_app.models.Course;
import com.Education_App.Xzpro.Education_app.models.RecyclerView.SectionDataModel;
import com.Education_App.Xzpro.Education_app.models.RecyclerView.SingleItemModel;
import com.Education_App.Xzpro.Education_app.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private APIService mApiService;
    FirebaseUser user;
    ArrayList<SectionDataModel> allSampleData;

    //инициализируем меню
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        //Обработчик события выбора элемента меню
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Intent intent;
            switch (item.getItemId()) {
                //если выбрана вкладка HOME
                case R.id.navigation_home:
                    return true;
                //если выбрана вкладка Profile
                case R.id.navigation_profile:
                    //запускаем активити
                    intent = new Intent(HomeActivity.this, ProfileActivity.class);
                    startActivity(intent);
                    finish();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);


        //ставим обработчик события на BottomNavigationView
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        mApiService = ApiUtils.getAPIService();
        allSampleData = new ArrayList<SectionDataModel>();
        user = FirebaseAuth.getInstance().getCurrentUser();

        getSupportActionBar().setTitle("Список курсов");
        /*
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            toolbar.setTitle("G PlayStore");

        }
        */

        loadCourseList();





    }


    private void setRecyclerView(){
        RecyclerView my_recycler_view = (RecyclerView) findViewById(R.id.my_recycler_view);

        my_recycler_view.setHasFixedSize(true);

        RecyclerViewDataAdapter adapter = new RecyclerViewDataAdapter(this, allSampleData);

        my_recycler_view.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));

        my_recycler_view.setAdapter(adapter);
    }

    public void loadCourseList(){
        String token = getSharedPreferences("authInfo",MODE_PRIVATE).getString("AccessToken","");
        mApiService.GetCourseNames(user.getUid(),token).enqueue(new Callback<ArrayList<Course>>() {
            @Override
            public void onResponse(Call<ArrayList<Course>> call, Response<ArrayList<Course>> response) {
                HashMap<String,SectionDataModel> modelHashMap = new HashMap<>();
                for (Course course:response.body()) {
                    SectionDataModel currentModel;
                    if (!modelHashMap.containsKey(course.getCategory())) {
                        SectionDataModel dm = new SectionDataModel();
                        dm.setHeaderTitle(course.getCategory());
                        modelHashMap.put(course.getCategory(),dm);
                        allSampleData.add(modelHashMap.get(course.getCategory()));
                    }
                    currentModel = modelHashMap.get(course.getCategory());
                    currentModel.addCourse(course);
                }

                setRecyclerView();
            }

            @Override
            public void onFailure(Call<ArrayList<Course>> call, Throwable t) {
                Log.w("CourseGetError",t.toString());
            }
        });
    }

    public void createDummyData() {
        for (int i = 1; i <= 5; i++) {

            SectionDataModel dm = new SectionDataModel();

            dm.setHeaderTitle("Section " + i);

            ArrayList<SingleItemModel> singleItem = new ArrayList<SingleItemModel>();
            for (int j = 0; j <= 5; j++) {
                singleItem.add(new SingleItemModel("Item " + j, "URL " + j));
            }

            //dm.setAllItemsInSection(singleItem);

            allSampleData.add(dm);

        }
    }


}
