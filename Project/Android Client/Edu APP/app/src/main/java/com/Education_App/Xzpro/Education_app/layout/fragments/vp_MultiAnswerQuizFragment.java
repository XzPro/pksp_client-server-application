package com.Education_App.Xzpro.Education_app.layout.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.Education_App.Xzpro.Education_app.activities.LessonDetailActivity;
import com.Education_App.Xzpro.Education_app.activities.OnTestAnsweredListener;
import com.Education_App.Xzpro.Education_app.models.Question;
import com.Education_App.Xzpro.Education_app.R;

import java.util.ArrayList;

public class vp_MultiAnswerQuizFragment extends Fragment implements View.OnClickListener {

    public static final String ARGUMENT_QUESTION = "arg_question";
    public static final String ARGUMENT_ANSWERS = "arg_answers";
    public static final String ARGUMENT_TRUE_ANSWER = "arg_true_answer";
    protected OnTestAnsweredListener subscriber;

    ArrayList<CheckBox> checkBoxGroup;
    TextView tv_result;
    public static vp_MultiAnswerQuizFragment newInstance(Question question, OnTestAnsweredListener subscriber) {
        vp_MultiAnswerQuizFragment pageFragment = new vp_MultiAnswerQuizFragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_QUESTION, question.getTitle());
        arguments.putStringArrayList(ARGUMENT_ANSWERS,question.getAnswers());
        arguments.putStringArrayList(ARGUMENT_TRUE_ANSWER,question.getRightAnswers());
        pageFragment.setArguments(arguments);
        pageFragment.subscriber = subscriber;
        return pageFragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkBoxGroup = new ArrayList<>();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_multi_answer_quiz, null);
        TextView tv_question = view.findViewById(R.id.tv_question);
        tv_result = view.findViewById(R.id.tv_result);
        checkBoxGroup.add(view.findViewById(R.id.cb_1));
        checkBoxGroup.add(view.findViewById(R.id.cb_2));
        checkBoxGroup.add(view.findViewById(R.id.cb_3));
        checkBoxGroup.add(view.findViewById(R.id.cb_4));
        for (int i = 0; i<4;i++){
            checkBoxGroup.get(i).setText(getArguments().getStringArrayList(ARGUMENT_ANSWERS).get(i));
        }
        view.findViewById(R.id.btn_answer).setOnClickListener(this);
        tv_question.setText(getArguments().getString(ARGUMENT_QUESTION));

        return view;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        checkBoxGroup.clear();
    }

    @Override
    public void onClick(View view) {
        ArrayList<String> right_answers = getArguments().getStringArrayList(ARGUMENT_TRUE_ANSWER);

        int cRightAnswers = 0;
        for (CheckBox checkBox:checkBoxGroup){
            if (checkBox.isChecked()){
                if (!right_answers.contains(checkBox.getText())){
                    cRightAnswers = 0;
                    break;
                } else {
                    cRightAnswers++;
                }
            }
        }

        String resultString;
        if (cRightAnswers == right_answers.size()){
            subscriber.getAnswer(true);
            resultString = "Правильный ответ! ";
        } else {
            subscriber.getAnswer(false);
            resultString = "Неправильный ответ! ";
            resultString += "Правильный ответ: ";
            for (String right_answer:right_answers) resultString += right_answer+", ";
            resultString.substring(0,resultString.length()-2);
        }

        if (subscriber instanceof LessonDetailActivity)
            tv_result.setText(resultString);
        view.setEnabled(false);
    }
}
