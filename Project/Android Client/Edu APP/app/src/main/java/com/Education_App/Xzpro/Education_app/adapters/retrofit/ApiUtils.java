package com.Education_App.Xzpro.Education_app.adapters.retrofit;

import com.Education_App.Xzpro.Education_app.Preferences;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 * Класс реализует сервис для RetrofitClient(в методе getAPIService() указываем класс интерфейса с запросами к сайту.)
 */

public class ApiUtils {


    private ApiUtils() {
    }

    public static APIService getAPIService() {

        return RetrofitClient.getClient(Preferences.BASE_URL).create(APIService.class);
    }
}
