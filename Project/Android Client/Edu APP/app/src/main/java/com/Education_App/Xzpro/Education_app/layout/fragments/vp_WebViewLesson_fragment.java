package com.Education_App.Xzpro.Education_app.layout.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

import androidx.fragment.app.Fragment;

import com.Education_App.Xzpro.Education_app.R;

public class vp_WebViewLesson_fragment extends Fragment {
    private WebView webView;
    static final String ARGUMENT_PAGE_NUMBER = "arg_page_number";
    static final String ARGUMENT_PAGE_CONTENT = "arg_page_content";
    String content;

   public static vp_WebViewLesson_fragment newInstance(String page) {
        vp_WebViewLesson_fragment pageFragment = new vp_WebViewLesson_fragment();
        Bundle arguments = new Bundle();
        arguments.putString(ARGUMENT_PAGE_CONTENT,page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

      //  pageNumber = getArguments().getInt(ARGUMENT_PAGE_NUMBER);
        content = getArguments().getString(ARGUMENT_PAGE_CONTENT);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.vp_textlesson_fragment, null);
        webView = (WebView) view.findViewById(R.id.webView);

        showDetailPost(content);

        return view;
    }

    private void showDetailPost(String content) {
        webView.setBackgroundColor(Color.WHITE);
        webView.setFocusableInTouchMode(false);
        webView.setFocusable(false);
        webView.getSettings().setDefaultTextEncodingName("UTF-8");
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);

        String mimeType = "text/html; charset=UTF-8";
        String encoding = "utf-8";
        String text = "<html><head>"
                + "<meta name=\"viewport\" content=\"width=device-width, Result-scalable=no\" />"
                + "<style type=\"text/css\">img {margin: 0.5% 0px !important;border: 0;font-size: 0;line-height: 0;max-width: 100%;}</style></head><body>"
                + content
                + "</body></html>";

        webView.loadData(text, mimeType, encoding);

    }
}
