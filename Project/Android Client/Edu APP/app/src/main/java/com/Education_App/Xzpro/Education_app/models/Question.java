package com.Education_App.Xzpro.Education_app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 *Класс, описывающий содержимое теста
 */

public class Question implements Parcelable {

    private int nquestion; //кол-во вопросов в тесте
    private int id;  //id лекции

    private int[] results;  //результаты по каждому вопросу теста
    @SerializedName("type")
    private String type;    // определяет тип вопроса
    @SerializedName("question")
    private String question;   //название теста
    @SerializedName("true_answer")
    private ArrayList<String> true_answers;
    @SerializedName("answers")
    private ArrayList<String> answers;   //содержимое вопросов

//конструкторы
    public Question(int nquestion, int id, String question, ArrayList<String> answers) {
        this.nquestion = nquestion;
        this.id = id;
        this.question = question;
        this.answers = answers;
    }

    public Question(Parcel parcel){
        answers = new ArrayList<>();
        true_answers = new ArrayList<>();
        question = parcel.readString();
        type = parcel.readString();
        parcel.readStringList(true_answers);
        parcel.readStringList(answers);
    }
    // Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(question);
        parcel.writeString(type);
        parcel.writeStringList(true_answers);
        parcel.writeStringList(answers);
    }

    public static final Parcelable.Creator<Question> CREATOR = new Parcelable.Creator<Question>(){
        @Override
        public Question createFromParcel(Parcel source) {
            return new Question(source);
        }

        @Override
        public Question[] newArray(int size) {
            return new Question[size];
        }
    };

    //Гетеры/сеттеры
    public int getNquestion() {
        return nquestion;
    }

    public void setNquestion(int nquestion) {
        this.nquestion = nquestion;
    }

    public String getType() {
        return type;
    }

    public int getid() {
        return id;
    }

    public void setSectionid(int id) {
        this.id = id;
    }

    public int[] getResults() {
        return results;
    }

    public void setResults(int[] results) {
        this.results = results;
    }

    public String getTitle() {
        return question;
    }

    public void setTitle(String title) {
        this.question = title;
    }

    public ArrayList<String> getAnswers() {
        return answers;
    }

    public void setAnswers(ArrayList<String> answers) {
        this.answers = answers;
    }

    public String getRightAnswer() {
        return true_answers.get(0);
    }

    public ArrayList<String> getRightAnswers() {return true_answers; }
}
