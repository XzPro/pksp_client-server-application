package com.Education_App.Xzpro.Education_app.models.RecyclerView;

import com.Education_App.Xzpro.Education_app.models.Course;

import java.util.ArrayList;

/**
 * Класс, описывающий модель данных для отображения в RecylerView.
 */
public class SectionDataModel {



    private String headerTitle;
    private ArrayList<Course> allItemsInSection;


    public SectionDataModel() {
        allItemsInSection = new ArrayList<>();

    }
    public SectionDataModel(String headerTitle, ArrayList<Course> allItemsInSection) {
        this.headerTitle = headerTitle;
        this.allItemsInSection = allItemsInSection;
    }



    public String getHeaderTitle() {
        return headerTitle;
    }

    public void setHeaderTitle(String headerTitle) {
        this.headerTitle = headerTitle;
    }

    public ArrayList<Course> getAllItemsInSection() {
        return allItemsInSection;
    }

    public void addCourse(Course course) {this.allItemsInSection.add(course);}

    public void setAllItemsInSection(ArrayList<Course> allItemsInSection) {
        this.allItemsInSection = allItemsInSection;
    }


}
