package com.Education_App.Xzpro.Education_app.models;

import com.google.gson.annotations.SerializedName;

import java.util.Calendar;
import java.util.Date;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 *Класс, описывающий атрибуты пользователя
 */

public class User {
    private String id;
    @SerializedName("username")
    private String UserName;
    private String Email;
    private Date RegDate;
    @SerializedName("token")
    private String Token;
    private String image;

    // getters and setters
    public String getUserName() {
        return UserName;
    }
    public void setUserName(String UserName) {
        this.UserName = UserName;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getEmail() {
        return Email;
    }
    public void setEmail(String Email) {
        this.Email = Email;
    }

    public String getToken(String id) {
        return Token;
    }
    public String getToken() {
        return Token;
    }
    public void setToken(String Token) {
        this.Token = Token;
    }

    public Date RegDate() {
        return RegDate;
    }
    public void setRegDate(String RegDate) {
        Date currentTime = Calendar.getInstance().getTime();
    }

    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
}
