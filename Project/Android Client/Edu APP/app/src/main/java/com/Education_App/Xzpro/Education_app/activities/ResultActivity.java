package com.Education_App.Xzpro.Education_app.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.Education_App.Xzpro.Education_app.R;
import com.Education_App.Xzpro.Education_app.adapters.recyclerview.ResultViewDataAdapter;

import java.util.ArrayList;



/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 * Класс предназначен для создания окна с результатами прохождения теста.
 * Включает функцию onCreate,которая создает activity и устанавливает toolbar
 *
 */
public class ResultActivity extends AppCompatActivity {
    private ArrayList<Boolean> userResults;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        userResults =(ArrayList<Boolean>) getIntent().getSerializableExtra("results");

        for (int i = userResults.size(); i<getIntent().getIntExtra("quizSize",0);i++)
            userResults.add(false);

        ResultViewDataAdapter adapter = new ResultViewDataAdapter(this,userResults);
        RecyclerView rv_results = findViewById(R.id.rv_results);
        rv_results.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rv_results.setAdapter(adapter);


        int rightCount = 0;
        for (Boolean result:userResults)
            if (result) rightCount++;

        TextView tv_score = findViewById(R.id.tv_score);
        tv_score.setText(String.format("Правильных ответов: %d/%d",rightCount,getIntent().getIntExtra("quizSize",0)));

        findViewById(R.id.btn_home).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ResultActivity.this,HomeActivity.class));
                finishAffinity();
            }
        });

    }


}
