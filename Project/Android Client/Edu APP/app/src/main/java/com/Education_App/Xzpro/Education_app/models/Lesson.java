package com.Education_App.Xzpro.Education_app.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 *Класс, описывающий содержимое лекций
 */

public class Lesson implements Parcelable {
    private String courseName;   //id курса, которому принадлежит урок
    private int id; // id лекции
    @SerializedName("title")
    private String title; // название лекции
    @SerializedName("sections")
    private ArrayList<String> sections;  //список тем-страниц в лекции
    private int result; //результат прохождения курса
    // Обновки
    @SerializedName("section_types")
    private List<String> section_types;
    @SerializedName("section_titles")
    private List<String> section_titles;
    // Обновки кончились
    private String content; //текст лекции
    private  String image;   //изображения
    @SerializedName("include_test")
    private  Boolean withTest; //наличие теста

    //Конструкторы
    public Lesson(int id, String title, ArrayList<String> sections, int result, String course) {
        this.id = id;
        this.title = title;
        this.sections = sections;
        this.result = result;
        this.courseName = course;
    }



    public Lesson(String title, ArrayList<String> sections) {
        this.title = title;
        this.sections = sections;
    }

    public Lesson(int id, String title, int result) {
        this.id = id;
        this.title = title;
        this.result = result;
    }

    public Lesson(Parcel parcel){
        sections = new ArrayList<>();
        section_titles = new ArrayList<>();
        section_types = new ArrayList<>();
        courseName = parcel.readString();
        withTest = parcel.readByte() != 0;
        parcel.readStringList(sections);
        parcel.readStringList(section_titles);
        parcel.readStringList(section_types);
    }

    // Parcelable

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(courseName);
        parcel.writeByte((byte)((withTest)?1:0));
        parcel.writeStringList(sections);
        parcel.writeStringList(section_titles);
        parcel.writeStringList(section_types);
    }

    public static final Parcelable.Creator<Lesson> CREATOR = new Parcelable.Creator<Lesson>(){
        @Override
        public Lesson createFromParcel(Parcel source) {
            return new Lesson(source);
        }

        @Override
        public Lesson[] newArray(int size) {
            return new Lesson[size];
        }
    };

    //Гетеры\сеттеры

    public String getContent() {
        return content;
    }
    public String getImage(){return image;}
    public String setImage(){return image;}
    public Boolean getWithTest(){return withTest;}

    public List<String> getSectionTypes() {
        return section_types;
    }

    public List<String> getSectionTitles() {
        return section_titles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<String> getSections() {
        return sections;
    }

    public void setSections(ArrayList<String> sections) {
        this.sections = sections;
    }

    public int getResult() {
        return result;
    }

    public void setResult(int result) {
        this.result = result;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseId(String courseName) {
        this.courseName = courseName;
    }

}
