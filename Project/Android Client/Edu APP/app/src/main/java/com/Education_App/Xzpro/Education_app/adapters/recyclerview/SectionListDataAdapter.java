package com.Education_App.Xzpro.Education_app.adapters.recyclerview;

/**
 * ??????? ??? ??????????? ?????? ?????? ??????. ???????????? ?? ViewPager
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.Education_App.Xzpro.Education_app.Preferences;
import com.Education_App.Xzpro.Education_app.activities.LessonListFromCourseActivity;
import com.Education_App.Xzpro.Education_app.models.Course;
import com.Education_App.Xzpro.Education_app.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;

public class SectionListDataAdapter extends RecyclerView.Adapter<SectionListDataAdapter.SingleItemRowHolder> {

    private ArrayList<Course> itemsList;
    private Context mContext;

    public SectionListDataAdapter(Context context, ArrayList<Course> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleItemRowHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.list_single_card, null);
        SingleItemRowHolder mh = new SingleItemRowHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleItemRowHolder holder, int i) {

        Course singleItem = itemsList.get(i);

        holder.tvTitle.setText(singleItem.getTitle());

        holder.mCourse = singleItem;

        Glide.with(mContext)
                .load(Preferences.BASE_URL+"/api/posts/getImage?image="+holder.mCourse.getCodename())
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .centerCrop()
                .placeholder(R.drawable.logo)
                .into(holder.itemImage);

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleItemRowHolder extends RecyclerView.ViewHolder {

        protected TextView tvTitle;

        protected ImageView itemImage;

        protected Course mCourse;


        public SingleItemRowHolder(View view) {
            super(view);

            this.tvTitle = (TextView) view.findViewById(R.id.tvTitle);
            this.itemImage = (ImageView) view.findViewById(R.id.itemImage);




            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent lessonList = new Intent(mContext, LessonListFromCourseActivity.class);
                    lessonList.putExtra("courseName",mCourse.getCodename());
                    lessonList.putExtra("courseTitle",mCourse.getTitle());
                    lessonList.putExtra("test_time",mCourse.getTestTime());
                    mContext.startActivity(lessonList);
                    Toast.makeText(v.getContext(), tvTitle.getText(), Toast.LENGTH_SHORT).show();

                }
            });


        }

    }

}