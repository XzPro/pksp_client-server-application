package com.Education_App.Xzpro.Education_app.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.Education_App.Xzpro.Education_app.models.Lesson;
import com.Education_App.Xzpro.Education_app.R;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 * Адаптер для отображения списка лекций

 */

public class LessonListAdapter extends BaseAdapter {
    List<Lesson> LessonList;
    Context context;
    boolean finalTestAccess;
    public LessonListAdapter(Context context, List<Lesson> postList, boolean finalTestAccess) {
        this.LessonList = postList;
        this.context = context;
        this.finalTestAccess = finalTestAccess;
    }

    @Override
    public int getCount() {
        return (finalTestAccess)?LessonList.size()+1:LessonList.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = LayoutInflater.from(context).inflate(R.layout.item_list_post, null, false);
        ViewHolder holder = new ViewHolder(view);
        if (i==LessonList.size()){
            holder.tvTitle.setText("Финальный тест");
        } else {
            holder.tvTitle.setText(LessonList.get(i).getTitle());
            Glide.with(context).load(LessonList.get(i).getImage())
                    .placeholder(R.mipmap.ic_launcher)
                    .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                    .into(holder.imgThumbnail);
        }
        return view;
    }

    public static class ViewHolder {
        public View rootView;
        public ImageView imgThumbnail;
        public TextView tvTitle;

        public ViewHolder(View rootView) {
            this.rootView = rootView;
            this.imgThumbnail = (ImageView) rootView.findViewById(R.id.imgThumbnail);
            this.tvTitle = (TextView) rootView.findViewById(R.id.tvTitle);
        }

    }
}
