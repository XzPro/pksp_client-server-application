package com.Education_App.Xzpro.Education_app.adapters.retrofit;

import com.Education_App.Xzpro.Education_app.models.Course;
import com.Education_App.Xzpro.Education_app.models.Lesson;
import com.Education_App.Xzpro.Education_app.models.Question;
import com.Education_App.Xzpro.Education_app.models.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * IDE: Android Studio
 * Name project: EduAPP
 *
 * Класс реализует интерфейс Retrofit для отправки запроса на сервер(
 * В интерфейсе задаются команды-запросы для сервера.
 * Команда комбинируется с базовым адресом сайта (baseUrl()) и получается полный путь к странице.)
 */
public interface APIService {
    /*
     * Retrofit отправляет запрос по нашему адресу
     * И метод getPostsDetails() возвращает информацию о темах .
    */

    // GET запрос списка тем на сервер
    // сервер возвращает JsonArray

    // запрос токена
    @GET("/api/firebase/getUserToken")
    Call<User> getToken(@Query("fb_id") String FirebaseID);


    // Запрос активных курсов
    @GET("/api/posts/getActiveCourses")
    Call<ArrayList<Course>> getActiveCourses(@Header("user-key") String userkey);

    // поучить список курсов
    @GET("/api/posts/getCourses")
    Call<ArrayList<Course>> GetCourseNames(@Query("user_id") String FirebaseID, @Header("user-key") String userkey);

    //    Получить Список лекций курса
    @GET("/api/posts/getLessons")
    Call<ArrayList<Lesson>> GetLessonsOfCourse(@Query("course") String name,@Header("user-key") String userkey);

    //    Получить тест урока
    @GET("/api/posts/getLessonTest")
    Call<ArrayList<Question>> getTest(@Query("course") String course, @Query("lesson_no") int lesson_no,@Header("user-key") String userkey);

    //    Получить результаты курса
    @GET("/api/posts/getCourseResults")
    Call<Course> getCourseResults(@Query("course") String course,@Header("user-key") String userkey);

    //    Получить итоговый тест курса
    @GET("/api/posts/getCourseTest")
    Call<ArrayList<Question>> getFinalTest(@Query("course") String course,@Header("user-key") String userkey);

    // put-request
    @PUT("/api/puts/putFinalResult")
    Call<String> putFinalTestResult(@Query("course") String course, @Header("user-key") String userkey);

    @PUT("/api/puts/putLessonTestResult")
    Call<String> putLessonTestResult(@Query("course") String course, @Query("lesson_no") int lesson_no, @Header("user-key") String userkey);


}
