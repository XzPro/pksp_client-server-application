package com.Education_App.Xzpro.Education_app.adapters.recyclerview;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.Education_App.Xzpro.Education_app.R;

import java.util.ArrayList;

/**
 * * Адаптер для отображения езультато в конце теста в виде списка
 */

public class ResultViewDataAdapter extends RecyclerView.Adapter<ResultViewDataAdapter.SingleResultHolder> {

    private ArrayList<Boolean> itemsList;
    private Context mContext;

    public ResultViewDataAdapter(Context context, ArrayList<Boolean> itemsList) {
        this.itemsList = itemsList;
        this.mContext = context;
    }

    @Override
    public SingleResultHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.activity_result_detail, null);
        SingleResultHolder mh = new SingleResultHolder(v);
        return mh;
    }

    @Override
    public void onBindViewHolder(SingleResultHolder holder, int i) {

        Boolean result = itemsList.get(i);
        holder.tvQueNo.setText(String.format("Вопрос №%d:",i+1));
        // TODO: Заменить цвета
        if (result) {
            holder.tvUserResult.setText("Правильный ответ");

            holder.tvUserResult.setBackgroundColor(Color.GREEN);
        } else {
            holder.tvUserResult.setText("Неправильный ответ");
            holder.tvUserResult.setBackgroundColor(Color.RED);
        }

    }

    @Override
    public int getItemCount() {
        return (null != itemsList ? itemsList.size() : 0);
    }

    public class SingleResultHolder extends RecyclerView.ViewHolder {

        protected TextView tvQueNo;

        protected TextView tvUserResult;


        public SingleResultHolder(View view) {
            super(view);

            this.tvQueNo = (TextView) view.findViewById(R.id.tv_que_no);
            this.tvUserResult = (TextView) view.findViewById(R.id.tv_user_answer);



        }

    }

}
